using Toybox.WatchUi as Ui;

class TimerDelegate extends Ui.BehaviorDelegate {

    hidden var data;

    function initialize(data) {
        BehaviorDelegate.initialize();
        self.data=data;
        models.get(data.name).setVisible(true);
    }

    function onSelect() {
        if(models.get(data.name).status==:Stop){
            models.get(data.name).resetTimer();
            Ui.popView(Ui.SLIDE_RIGHT);
        }
    }

    function onMenu(){
        var menu = new AlarmMenu (data);
        Ui.pushView(menu,new AlarmMenuDelegate(menu) ,  Ui.SLIDE_RIGHT );
    }

    function onBack() {
        models.get(data.name).setVisible(false);
    }
}
