using Toybox.WatchUi as Ui;

class HomeMenuDelegate extends MenuDelegate {

    function initialize(menu) {
        MenuDelegate.initialize(menu);
    }

    function onMenuItem(item) {
        if(item.id==:Add){
            if (Ui has :TextPicker) {
                Ui.pushView(new Ui.TextPicker(""), new AddElementPicker(), Ui.SLIDE_LEFT);
            }
        }else{
            var menu = new ElementMenu (item.data);
             Ui.pushView(menu,new ElementMenuDelegate(menu) ,  Ui.SLIDE_LEFT );
        }
    }

    function onBack() {
        var values = models.values();
        for( var i=0; i<values.size();i++){
            values[i].resetTimer();
        }
    }
}
