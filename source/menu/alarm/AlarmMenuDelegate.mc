using Toybox.WatchUi as Ui;

class AlarmMenuDelegate extends MenuDelegate {

    function initialize(menu) {
        MenuDelegate.initialize(menu);
    }

    function onMenuItem(item) {
        if(item.id ==:Beep){
            item.data.beep=!item.data.beep;
        }else if(item.id ==:Beep5s){
            item.data.beep5s=!item.data.beep5s;
        }else if(item.id ==:Vibrate){
            item.data.vibrate=!item.data.vibrate;
        }
    }

}
